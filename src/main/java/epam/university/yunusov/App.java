package epam.university.yunusov;

import epam.university.yunusov.model.bean.BeanA;
import epam.university.yunusov.model.bean.BeanB;
import epam.university.yunusov.model.bean.BeanC;
import epam.university.yunusov.model.bean.BeanD;
import epam.university.yunusov.model.bean.BeanE;
import epam.university.yunusov.model.bean.BeanF;
import epam.university.yunusov.model.configuration.MainConfig;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class App {

  public static void main(String[] args) {
    ApplicationContext context = new AnnotationConfigApplicationContext(MainConfig.class);

    System.out.println(context.getBean("beanA_valuesOf_beanB_beanC", BeanA.class));
    System.out.println(context.getBean("beanA_valuesOf_beanB_beanD", BeanA.class));
    System.out.println(context.getBean("beanA_valuesOf_beanC_beanD", BeanA.class));
    System.out.println(context.getBean(BeanB.class));
    System.out.println(context.getBean(BeanC.class));
    System.out.println(context.getBean(BeanD.class));
    System.out.println(context.getBean("beanE1", BeanE.class));
    System.out.println(context.getBean("beanE2", BeanE.class));
    System.out.println(context.getBean("beanE3", BeanE.class));
    try {
      System.out.println(context.getBean(BeanF.class));
    } catch (NoSuchBeanDefinitionException e) {
      System.out.println("BeanE initialized as @Lazy");
    }
  }

}
