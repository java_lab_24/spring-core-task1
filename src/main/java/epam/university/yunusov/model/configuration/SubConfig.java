package epam.university.yunusov.model.configuration;

import epam.university.yunusov.model.bean.BeanA;
import epam.university.yunusov.model.bean.BeanB;
import epam.university.yunusov.model.bean.BeanC;
import epam.university.yunusov.model.bean.BeanD;
import epam.university.yunusov.model.bean.BeanE;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SubConfig {

  @Bean
  public BeanB getBeanB() {
    return new BeanB();
  }

  @Bean
  public BeanC getBeanC() {
    return new BeanC();
  }

  @Bean
  public BeanD getBeaD() {
    return new BeanD();
  }
}
