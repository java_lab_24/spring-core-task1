package epam.university.yunusov.model.configuration;

import epam.university.yunusov.model.bean.BeanA;
import epam.university.yunusov.model.bean.BeanB;
import epam.university.yunusov.model.bean.BeanC;
import epam.university.yunusov.model.bean.BeanD;
import epam.university.yunusov.model.bean.BeanE;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;

@Configuration
@Import(SubConfig.class)
@PropertySource("bean.properties")
public class MainConfig {

  @Bean(name = "beanA_valuesOf_beanB_beanC")
  public BeanA getBeanA1(BeanB beanB, BeanC beanC) {
    BeanA beanA = new BeanA();
    beanA.setName(beanB.getName());
    beanA.setValue(beanC.getValue());
    return beanA;
  }

  @Bean(name = "beanA_valuesOf_beanB_beanD")
  public BeanA getBeanA2(BeanB beanB, BeanD beanD) {
    BeanA beanA = new BeanA();
    beanA.setName(beanB.getName());
    beanA.setValue(beanD.getValue());
    return beanA;
  }

  @Bean(name = "beanA_valuesOf_beanC_beanD")
  public BeanA getBeanA3(BeanC beanC, BeanD beanD) {
    BeanA beanA = new BeanA();
    beanA.setName(beanC.getName());
    beanA.setValue(beanD.getValue());
    return beanA;
  }

  @Bean (name = "beanE1")
  public BeanE getBeanE1(@Qualifier("beanA_valuesOf_beanB_beanC") BeanA beanA) {
    BeanE beanE = new BeanE();
    beanE.setName(beanA.getName());
    beanE.setValue(beanA.getValue());
    return beanE;
  }

  @Bean (name = "beanE2")
  public BeanE getBeanE2(@Qualifier("beanA_valuesOf_beanB_beanD") BeanA beanA) {
    BeanE beanE = new BeanE();
    beanE.setName(beanA.getName());
    beanE.setValue(beanA.getValue());
    return beanE;
  }

  @Bean (name = "beanE3")
  public BeanE getBeanE3(@Qualifier("beanA_valuesOf_beanC_beanD") BeanA beanA) {
    BeanE beanE = new BeanE();
    beanE.setName(beanA.getName());
    beanE.setValue(beanA.getValue());
    return beanE;
  }
}
