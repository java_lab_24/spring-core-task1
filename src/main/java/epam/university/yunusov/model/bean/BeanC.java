package epam.university.yunusov.model.bean;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(1)
public class BeanC {

  @Value("${beanC.name}")
  private String name;
  @Value("${beanC.value}")
  private int value;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getValue() {
    return value;
  }

  public void setValue(int value) {
    this.value = value;
  }

  @PostConstruct
  public void init() {
    System.out.println("Initializing class " + BeanC.class.getName());
}

  @PreDestroy
  public void destroy() {
    System.out.println("Destroying class " + BeanC.class.getName());
  }

  @Override
  public String toString() {
    return "BeanC{" +
        "name='" + name + '\'' +
        ", value=" + value +
        '}';
  }
}
