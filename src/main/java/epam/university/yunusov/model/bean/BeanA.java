package epam.university.yunusov.model.bean;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

@Component
public class BeanA implements InitializingBean, DisposableBean {

  private String name;
  private int value;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getValue() {
    return value;
  }

  public void setValue(int value) {
    this.value = value;
  }

  @Override
  public String toString() {
    return "BeanA{" +
        "name='" + name + '\'' +
        ", value=" + value +
        '}';
  }

  @Override
  public void destroy() throws Exception {
    System.out.println("BeanA destroy method");
  }

  @Override
  public void afterPropertiesSet() throws Exception {
    System.out.println("BeanA afterPropertiesSet method");
  }
}
